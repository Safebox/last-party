﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Artemis.Interface;

namespace Last_Party.ArtemisComponents
{
    public class Transform : IComponent
    {
        //Variables
        private Vector2 lastPos;
        private Vector2 pos;
        private int rot;

        //Properties
        public Vector2 LastPosition
        {
            get
            {
                return lastPos;
            }
            set
            {
                lastPos = value;
            }
        }
        public Vector2 Position
        {
            get
            {
                return pos;
            }
            set
            {
                pos = value;
            }
        }
        public int Rotation
        {
            get
            {
                return rot;
            }
            set
            {
                rot = value;
            }
        }
        public Vector2 Velocity
        {
            get
            {
                return pos - lastPos;
            }
        }

        //Constructors
        public Transform(Vector2 position, int rotation)
        {
            pos = position;
            rot = rotation;
        }
    }
}
