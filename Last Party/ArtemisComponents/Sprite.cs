﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Graphics;
using Artemis.Interface;

namespace Last_Party.ArtemisComponents
{
    public class Sprite : IComponent
    {
        //Variables
        private Texture2D sprTex;
        private float animTime;
        
        //Properties
        public Texture2D SpriteTexture
        {
            get
            {
                return sprTex;
            }
        }
        public int AnimFrame
        {
            get
            {
                return (int)Math.Round(Math.Sin(2 * Math.PI * animTime));
            }
        }
        public float AnimTime
        {
            get
            {
                return animTime;
            }
        }

        //Constructors
        public Sprite(Texture2D texture)
        {
            sprTex = texture;
        }

        //Methods
        public void SetTime(float value)
        {
            animTime = value;
        }
        public void IncrementTime(float value)
        {
            animTime += value;
        }
        public void ResetTime()
        {
            animTime = 0;
        }
    }
}
