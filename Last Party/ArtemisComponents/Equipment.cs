﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Artemis.Interface;

namespace Last_Party.ArtemisComponents
{
    public class Equipment : IComponent
    {
        //Variables
        public bool IsWeaponEquipped;
        public GameStates.EquippedAbility SelectedAbility;
        public GameStates.Weapons SelectedWeapon;
        public GameStates.ActivePowers SelectedPower;
        public Dictionary<GameStates.ActivePowers, byte> ActivePowerLevels;
        public Dictionary<GameStates.PassivePowers, byte> PassivePowerLevels;
        private float manaTime;
        private GameStates.ActivePowers lastPower;
        private GameStates.ActivePowers? activePower;
        private float weaponTime;
        private GameStates.Weapons? activeWeapon;

        //Properties
        public float ManaTime
        {
            get
            {
                return manaTime;
            }
        }
        public int GetLastPowerLevel
        {
            get
            {
                return ActivePowerLevels[lastPower];
            }
        }
        public GameStates.ActivePowers? CurrentPower
        {
            get
            {
                return activePower;
            }
        }
        public float WeaponTime
        {
            get
            {
                return weaponTime;
            }
        }
        public GameStates.Weapons? CurrentWeapon
        {
            get
            {
                return activeWeapon;
            }
        }

        //Constructors
        public Equipment()
        {
            IsWeaponEquipped = false;
            SelectedAbility = GameStates.EquippedAbility.Power;
            SelectedWeapon = GameStates.Weapons.Heart;
            SelectedPower = GameStates.ActivePowers.Blink;
            ActivePowerLevels = new Dictionary<GameStates.ActivePowers, byte>
            {
                { GameStates.ActivePowers.Blink, 1 },
                { GameStates.ActivePowers.DarkVision, 1 },
                { GameStates.ActivePowers.DevouringSwarm, 1 },
                { GameStates.ActivePowers.Possession, 0 },
                { GameStates.ActivePowers.BendTime, 0 },
                { GameStates.ActivePowers.WindBlast, 0 }
            };
            PassivePowerLevels = new Dictionary<GameStates.PassivePowers, byte>
            {
                { GameStates.PassivePowers.Vitality, 0 },
                { GameStates.PassivePowers.BloodThirsty, 0 },
                { GameStates.PassivePowers.Agility, 0 },
                { GameStates.PassivePowers.ShadowKill, 0 }
            };
            manaTime = 1f;
            weaponTime = 1f;
        }

        //Methods
        public void IncrementTime(float value)
        {
            manaTime += value;
        }
        public void ResetTime()
        {
            manaTime = 1;
            activePower = null;
        }
        public void StartTime(GameStates.ActivePowers power)
        {
            manaTime = 0;
            activePower = power;
        }
        public void SetLastPower(GameStates.ActivePowers last)
        {
            lastPower = last;
        }

        public void IncrementWeaponTime(float value)
        {
            weaponTime += value;
        }
        public void ResetWeaponTime()
        {
            weaponTime = 1;
        }
        public void StartWeaponTime(GameStates.Weapons weapon)
        {
            weaponTime = 0;
            activeWeapon = weapon;
        }
    }
}
