﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Artemis.Interface;

namespace Last_Party.ArtemisComponents
{
    public class NPC : IComponent
    {
        private string type;
        private bool killed;
        private bool unconscious;

        public string Name
        {
            get
            {
                return type;
            }
        }
        public bool HasBeenKilled
        {
            get
            {
                return killed;
            }
            set
            {
                killed = value;
            }
        }
        public bool IsUnconscious
        {
            get
            {
                return unconscious;
            }
            set
            {
                unconscious = value;
            }
        }

        public NPC(string name)
        {
            type = name;
        }
    }
}
