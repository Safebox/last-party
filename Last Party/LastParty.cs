﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using System;
using System.IO;
using Newtonsoft.Json;
using Artemis;
using TiledSharp;

namespace Last_Party
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class LastParty : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Config config;
        AssetLoader assets;
        GameStates gameStates;

        EntityWorld world;
        EntityWorld enemyWorld;
        EntityWorld npcWorld;
        ArtemisSystems.SystemSpriteAnimation SystemAnimation;
        ArtemisSystems.SystemPlayerMovement SystemPlayerMovement;
        ArtemisSystems.SystemPower SystemPower;
        ArtemisSystems.SystemWeapon SystemWeapon;

        RenderTarget2D BackLayer;
        RenderTarget2D ObjectLayer;
        RenderTarget2D PlayerLayer;
        RenderTarget2D OverLayer;

        public LastParty()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //Initialise Layers
            BackLayer = new RenderTarget2D(GraphicsDevice, 240, 160, false, graphics.PreferredBackBufferFormat, graphics.PreferredDepthStencilFormat, 0, RenderTargetUsage.DiscardContents);
            ObjectLayer = new RenderTarget2D(GraphicsDevice, 240, 160, false, graphics.PreferredBackBufferFormat, graphics.PreferredDepthStencilFormat, 0, RenderTargetUsage.PreserveContents);
            PlayerLayer = new RenderTarget2D(GraphicsDevice, 240, 160, false, graphics.PreferredBackBufferFormat, graphics.PreferredDepthStencilFormat, 0, RenderTargetUsage.PreserveContents);
            OverLayer = new RenderTarget2D(GraphicsDevice, 240, 160, false, graphics.PreferredBackBufferFormat, graphics.PreferredDepthStencilFormat, 0, RenderTargetUsage.PreserveContents);

            //Set Window Size
            graphics.PreferredBackBufferWidth = 240;
            graphics.PreferredBackBufferHeight = 160;
            graphics.ApplyChanges();

            //Initialise Game States
            gameStates = new GameStates();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Read / Write Config File
            config = new Config();
            if (File.Exists(Directory.GetCurrentDirectory() + @"\Config.json"))
            {
                config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(Directory.GetCurrentDirectory() + @"\Config.json"));
            }
            else
            {
                File.WriteAllText(Directory.GetCurrentDirectory() + @"\Config.json", JsonConvert.SerializeObject(config, Formatting.Indented));
            }

            //Read / Write Asset Loader File
            assets = new AssetLoader(Content, GraphicsDevice, config);

            //Initialise Entity World
            world = new EntityWorld();
            SystemAnimation = new ArtemisSystems.SystemSpriteAnimation();
            SystemPlayerMovement = new ArtemisSystems.SystemPlayerMovement();
            SystemPower = new ArtemisSystems.SystemPower();
            SystemWeapon = new ArtemisSystems.SystemWeapon();

            //Initialise NPCs
            Entity player = world.CreateEntity();
            ArtemisComponents.Transform playerTrans = new ArtemisComponents.Transform(new Vector2(38, 27), 3);
            playerTrans.LastPosition = playerTrans.Position;
            ArtemisComponents.Sprite playerSprite = new ArtemisComponents.Sprite(assets.PlayerSprite);
            ArtemisComponents.Equipment playerEquip = new ArtemisComponents.Equipment();

            player.AddComponent<ArtemisComponents.Transform>(playerTrans);
            player.AddComponent<ArtemisComponents.Sprite>(playerSprite);
            player.AddComponent<ArtemisComponents.Equipment>(playerEquip);

            enemyWorld = new EntityWorld();
            foreach (TmxObject o in assets.map.ObjectGroups["Enemies"].Objects)
            {
                Entity enemy = enemyWorld.CreateEntity();
                ArtemisComponents.Transform enemyTrans = new ArtemisComponents.Transform(new Vector2((int)Math.Round(o.X / 16f), (int)Math.Round(o.Y / 16f) - 1), int.Parse(o.Properties["Rot"]));
                enemyTrans.LastPosition = enemyTrans.Position;
                ArtemisComponents.Sprite enemySprite = new ArtemisComponents.Sprite(o.Type == "G" ? assets.GuardSprite : assets.MusicGuardSprite);

                enemy.AddComponent<ArtemisComponents.Transform>(enemyTrans);
                enemy.AddComponent<ArtemisComponents.Sprite>(enemySprite);
                enemy.AddComponent<ArtemisComponents.NPC>(new ArtemisComponents.NPC(o.Type));
            }

            npcWorld = new EntityWorld();
            foreach (TmxObject o in assets.map.ObjectGroups["Guests"].Objects)
            {
                Entity guest = npcWorld.CreateEntity();
                ArtemisComponents.Transform guestTrans = new ArtemisComponents.Transform(new Vector2((int)Math.Round(o.X / 16f), (int)Math.Round(o.Y / 16f) - 1), int.Parse(o.Properties["Rot"]));
                guestTrans.LastPosition = guestTrans.Position;

                Texture2D tex = new Texture2D(GraphicsDevice, 1, 1);
                switch (o.Type)
                {
                    case "1":
                        tex = assets.Guest1Sprite;
                        break;
                    case "2":
                        tex = assets.Guest2Sprite;
                        break;
                    case "3":
                        tex = assets.Guest3Sprite;
                        break;
                    case "T1":
                        tex = assets.Target1Sprite;
                        break;
                    case "T2":
                        tex = assets.Target2Sprite;
                        break;
                    case "T3":
                        tex = assets.Target3Sprite;
                        break;
                }
                ArtemisComponents.Sprite guestSprite = new ArtemisComponents.Sprite(tex);

                guest.AddComponent<ArtemisComponents.Transform>(guestTrans);
                guest.AddComponent<ArtemisComponents.Sprite>(guestSprite);
                guest.AddComponent<ArtemisComponents.NPC>(new ArtemisComponents.NPC(o.Type));
                if (o.Type != "T1" && o.Type != "T2" && o.Type != "T3")
                {
                    guest.AddComponent<ArtemisComponents.Hint>(new ArtemisComponents.Hint());
                }
                else
                {
                    guest.AddComponent<ArtemisComponents.Target>(new ArtemisComponents.Target());
                }
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            //Finite State Machine
            switch (gameStates.GameplayState)
            {
                case GameStates.PlayStates.MainMenu:
                    //Update Music
                    if (MediaPlayer.State == MediaState.Stopped)
                    {
                        MediaPlayer.Play(assets.MenuTheme);
                        MediaPlayer.Volume = config.MusicVolume / 100f;
                    }

                    //Update Selected Menu
                    gameStates.NewKey = Keyboard.GetState();
                    if (gameStates.LastKey.IsKeyUp(config.ButtonDOWN) && gameStates.NewKey.IsKeyDown(config.ButtonDOWN))
                    {
                        switch (gameStates.SelectedMenuItem)
                        {
                            case GameStates.MainMenuStates.NewGame:
                                gameStates.SelectedMenuItem = GameStates.MainMenuStates.LoadGame;
                                break;
                            case GameStates.MainMenuStates.LoadGame:
                                gameStates.SelectedMenuItem = GameStates.MainMenuStates.Options;
                                break;
                        }
                    }
                    else if (gameStates.LastKey.IsKeyUp(config.ButtonUP) && gameStates.NewKey.IsKeyDown(config.ButtonUP))
                    {
                        switch (gameStates.SelectedMenuItem)
                        {
                            case GameStates.MainMenuStates.LoadGame:
                                gameStates.SelectedMenuItem = GameStates.MainMenuStates.NewGame;
                                break;
                            case GameStates.MainMenuStates.Options:
                                gameStates.SelectedMenuItem = GameStates.MainMenuStates.LoadGame;
                                break;
                        }
                    }
                    else if (gameStates.LastKey.IsKeyUp(config.ButtonA) && gameStates.NewKey.IsKeyDown(config.ButtonA))
                    {
                        switch (gameStates.SelectedMenuItem)
                        {
                            case GameStates.MainMenuStates.NewGame:
                                MediaPlayer.Stop();
                                gameStates.GameplayState = GameStates.PlayStates.Playing;
                                for (int i = 0; i < world.EntityManager.ActiveEntities.Count; i++)
                                {
                                    if (world.EntityManager.ActiveEntities[i].GetComponent<ArtemisComponents.Equipment>() != null)
                                    {
                                        File.WriteAllText(Directory.GetCurrentDirectory() + @"\SaveGame.json", JsonConvert.SerializeObject(world.EntityManager.ActiveEntities[i].GetComponent<ArtemisComponents.Equipment>(), Formatting.Indented));
                                    }
                                }
                                gameStates.Target = new Random().Next(0, 3);
                                break;
                            case GameStates.MainMenuStates.LoadGame:
                                MediaPlayer.Stop();
                                gameStates.GameplayState = GameStates.PlayStates.Playing;
                                for (int i = 0; i < world.EntityManager.ActiveEntities.Count; i++)
                                {
                                    if (world.EntityManager.ActiveEntities[i].GetComponent<ArtemisComponents.Equipment>() != null)
                                    {
                                        if (File.Exists(Directory.GetCurrentDirectory() + @"\SaveGame.json"))
                                        {
                                            world.EntityManager.ActiveEntities[i].RemoveComponent<ArtemisComponents.Equipment>();
                                            world.EntityManager.ActiveEntities[i].AddComponent<ArtemisComponents.Equipment>(JsonConvert.DeserializeObject<ArtemisComponents.Equipment>(File.ReadAllText(Directory.GetCurrentDirectory() + @"\SaveGame.json")));
                                        }
                                    }
                                }
                                break;
                            case GameStates.MainMenuStates.Options:
                                gameStates.GameplayState = GameStates.PlayStates.Options;
                                break;
                        }
                    }
                    gameStates.LastKey = gameStates.NewKey;
                    break;
                case GameStates.PlayStates.Options:
                    //Update Music
                    if (MediaPlayer.State == MediaState.Stopped)
                    {
                        MediaPlayer.Play(assets.MenuTheme);
                        MediaPlayer.Volume = config.MusicVolume / 100f;
                    }

                    //Update Selected Menu
                    gameStates.NewKey = Keyboard.GetState();
                    if (gameStates.LastKey.IsKeyUp(config.ButtonDOWN) && gameStates.NewKey.IsKeyDown(config.ButtonDOWN))
                    {
                        switch (gameStates.SelectedOptionItem)
                        {
                            case GameStates.OptionStates.SoundVolume:
                                gameStates.SelectedOptionItem = GameStates.OptionStates.MusicVolume;
                                break;
                            case GameStates.OptionStates.MusicVolume:
                                gameStates.SelectedOptionItem = GameStates.OptionStates.Back;
                                break;
                        }
                    }
                    else if (gameStates.LastKey.IsKeyUp(config.ButtonUP) && gameStates.NewKey.IsKeyDown(config.ButtonUP))
                    {
                        switch (gameStates.SelectedOptionItem)
                        {
                            case GameStates.OptionStates.MusicVolume:
                                gameStates.SelectedOptionItem = GameStates.OptionStates.SoundVolume;
                                break;
                            case GameStates.OptionStates.Back:
                                gameStates.SelectedOptionItem = GameStates.OptionStates.MusicVolume;
                                break;
                        }
                    }
                    else if (gameStates.LastKey.IsKeyUp(config.ButtonLEFT) && gameStates.NewKey.IsKeyDown(config.ButtonLEFT))
                    {
                        switch (gameStates.SelectedOptionItem)
                        {
                            case GameStates.OptionStates.SoundVolume:
                                config.SoundVolume -= (config.SoundVolume > 0 ? 10 : 0);
                                break;
                            case GameStates.OptionStates.MusicVolume:
                                config.MusicVolume -= (config.MusicVolume > 0 ? 10 : 0);
                                break;
                        }
                        MediaPlayer.Volume = config.MusicVolume / 100f;
                        File.WriteAllText(Directory.GetCurrentDirectory() + @"\Config.json", JsonConvert.SerializeObject(config, Formatting.Indented));
                    }
                    else if (gameStates.LastKey.IsKeyUp(config.ButtonRIGHT) && gameStates.NewKey.IsKeyDown(config.ButtonRIGHT))
                    {
                        switch (gameStates.SelectedOptionItem)
                        {
                            case GameStates.OptionStates.SoundVolume:
                                config.SoundVolume += (config.SoundVolume < 100 ? 10 : 0);
                                break;
                            case GameStates.OptionStates.MusicVolume:
                                config.MusicVolume += (config.MusicVolume < 100 ? 10 : 0);
                                break;
                        }
                        MediaPlayer.Volume = config.MusicVolume / 100f;
                        File.WriteAllText(Directory.GetCurrentDirectory() + @"\Config.json", JsonConvert.SerializeObject(config, Formatting.Indented));
                    }
                    else if (gameStates.LastKey.IsKeyUp(config.ButtonA) && gameStates.NewKey.IsKeyDown(config.ButtonA))
                    {
                        switch (gameStates.SelectedOptionItem)
                        {
                            case GameStates.OptionStates.Back:
                                gameStates.GameplayState = GameStates.PlayStates.MainMenu;
                                gameStates.SelectedOptionItem = GameStates.OptionStates.SoundVolume;
                                break;
                        }
                    }
                    else if (gameStates.LastKey.IsKeyUp(config.ButtonB) && gameStates.NewKey.IsKeyDown(config.ButtonB))
                    {
                        gameStates.GameplayState = GameStates.PlayStates.MainMenu;
                        gameStates.SelectedOptionItem = GameStates.OptionStates.SoundVolume;
                    }
                    gameStates.LastKey = gameStates.NewKey;
                    break;
                case GameStates.PlayStates.Quickwheel:
                    //Update Music
                    if (MediaPlayer.State == MediaState.Stopped)
                    {
                        MediaPlayer.Play(assets.MenuTheme);
                        MediaPlayer.Volume = config.MusicVolume / 100f;
                    }

                    //Update Input
                    gameStates.NewKey = Keyboard.GetState();
                    if (gameStates.NewKey.IsKeyUp(config.ButtonL))
                    {
                        gameStates.GameplayState = GameStates.PlayStates.Playing;
                    }
                    else
                    {
                        if (gameStates.KeyReset == 15)
                        {
                            foreach (Entity e in world.EntityManager.ActiveEntities)
                            {
                                if (gameStates.LastKey.IsKeyUp(config.ButtonDOWN) && gameStates.NewKey.IsKeyDown(config.ButtonDOWN))
                                {
                                    if (e.GetComponent<ArtemisComponents.Equipment>().SelectedAbility == GameStates.EquippedAbility.Power)
                                    {
                                        e.GetComponent<ArtemisComponents.Equipment>().SelectedAbility = GameStates.EquippedAbility.Weapon;
                                    }
                                }
                                else if (gameStates.LastKey.IsKeyUp(config.ButtonLEFT) && gameStates.NewKey.IsKeyDown(config.ButtonLEFT))
                                {
                                    if (e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon != GameStates.Weapons.Heart)
                                    {
                                        e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon = (e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon - 1);
                                    }
                                    if (e.GetComponent<ArtemisComponents.Equipment>().SelectedPower != GameStates.ActivePowers.Blink)
                                    {
                                        e.GetComponent<ArtemisComponents.Equipment>().SelectedPower = (e.GetComponent<ArtemisComponents.Equipment>().SelectedPower - 1);
                                    }
                                }
                                else if (gameStates.LastKey.IsKeyUp(config.ButtonRIGHT) && gameStates.NewKey.IsKeyDown(config.ButtonRIGHT))
                                {
                                    if (e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon != GameStates.Weapons.Crossbow_Sleep)
                                    {
                                        e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon = (e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon + 1);
                                    }
                                    if (e.GetComponent<ArtemisComponents.Equipment>().SelectedPower != GameStates.ActivePowers.DevouringSwarm)
                                    {
                                        e.GetComponent<ArtemisComponents.Equipment>().SelectedPower = (e.GetComponent<ArtemisComponents.Equipment>().SelectedPower + 1);
                                    }
                                }
                                else if (gameStates.LastKey.IsKeyUp(config.ButtonUP) && gameStates.NewKey.IsKeyDown(config.ButtonUP))
                                {
                                    if (e.GetComponent<ArtemisComponents.Equipment>().SelectedAbility == GameStates.EquippedAbility.Weapon)
                                    {
                                        e.GetComponent<ArtemisComponents.Equipment>().SelectedAbility = GameStates.EquippedAbility.Power;
                                    }
                                }
                            }
                        }
                        else
                        {
                            gameStates.KeyReset = Math.Min(gameStates.KeyReset + 1, 15);
                        }
                        gameStates.LastKey = gameStates.NewKey;
                    }
                    break;
                case GameStates.PlayStates.Playing:
                    //Update Music
                    if (MediaPlayer.State == MediaState.Stopped)
                    {
                        switch (gameStates.CurrentSong % 2)
                        {
                            case 0:
                                MediaPlayer.Play(assets.BackgroundMusic1, gameStates.LastTime);
                                break;
                            case 1:
                                MediaPlayer.Play(assets.BackgroundMusic2, gameStates.LastTime);
                                break;
                        }
                        MediaPlayer.Volume = config.MusicVolume / 100f;
                    }
                    else if (MediaPlayer.PlayPosition == assets.BackgroundMusic1.Duration - new TimeSpan(0, 0, 1))
                    {
                        gameStates.CurrentSong++;
                        gameStates.LastTime = new TimeSpan();
                    }

                    //Update Input
                    gameStates.NewKey = Keyboard.GetState();
                    if (gameStates.KeyReset == 15)
                    {
                        if (gameStates.PlayerVeloc == new Vector2())
                        {
                            if (gameStates.NewKey.IsKeyDown(config.ButtonDOWN))
                            {
                                gameStates.PlayerVeloc = new Vector2(0, 1);
                                gameStates.PayerRot = 0;
                                gameStates.KeyReset = 0;
                            }
                            else if (gameStates.NewKey.IsKeyDown(config.ButtonLEFT))
                            {
                                gameStates.PlayerVeloc = new Vector2(-1, 0);
                                gameStates.PayerRot = 1;
                                gameStates.KeyReset = 0;
                            }
                            else if (gameStates.NewKey.IsKeyDown(config.ButtonRIGHT))
                            {
                                gameStates.PlayerVeloc = new Vector2(1, 0);
                                gameStates.PayerRot = 2;
                                gameStates.KeyReset = 0;
                            }
                            else if (gameStates.NewKey.IsKeyDown(config.ButtonUP))
                            {
                                gameStates.PlayerVeloc = new Vector2(0, -1);
                                gameStates.PayerRot = 3;
                                gameStates.KeyReset = 0;
                            }
                        }
                        if (gameStates.NewKey.IsKeyDown(config.ButtonL))
                        {
                            gameStates.GameplayState = GameStates.PlayStates.Quickwheel;
                        }
                        if (gameStates.LastKey.IsKeyUp(config.ButtonR) && gameStates.NewKey.IsKeyDown(config.ButtonR))
                        {
                            foreach (Entity e in world.EntityManager.ActiveEntities)
                            {
                                e.GetComponent<ArtemisComponents.Equipment>().IsWeaponEquipped = !e.GetComponent<ArtemisComponents.Equipment>().IsWeaponEquipped;
                            }
                        }
                        foreach (Entity e in world.EntityManager.ActiveEntities)
                        {
                            if (e.GetComponent<ArtemisComponents.Equipment>().IsWeaponEquipped)
                            {
                                if (gameStates.LastKey.IsKeyUp(config.ButtonB) && gameStates.NewKey.IsKeyDown(config.ButtonB))
                                {
                                    switch (e.GetComponent<ArtemisComponents.Equipment>().SelectedAbility)
                                    {
                                        case GameStates.EquippedAbility.Power:
                                            if (e.GetComponent<ArtemisComponents.Equipment>().ManaTime == 1f)
                                            {
                                                switch (e.GetComponent<ArtemisComponents.Equipment>().SelectedPower)
                                                {
                                                    case GameStates.ActivePowers.Blink:
                                                        SystemPower.ProcessBlink(e, ref gameStates.PlayerVeloc, assets.map.Layers["Blocking"]);
                                                        break;
                                                    case GameStates.ActivePowers.DarkVision:
                                                        e.GetComponent<ArtemisComponents.Equipment>().StartTime(GameStates.ActivePowers.DarkVision);
                                                        break;
                                                    case GameStates.ActivePowers.DevouringSwarm:
                                                        SystemPower.ProcessRats(e, enemyWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                                        SystemPower.ProcessRats(e, npcWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                                        break;
                                                }
                                            }
                                            break;
                                        case GameStates.EquippedAbility.Weapon:
                                            if (e.GetComponent<ArtemisComponents.Equipment>().WeaponTime == 1f)
                                            {
                                                switch (e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon)
                                                {
                                                    case GameStates.Weapons.Heart:
                                                        SystemWeapon.ProcessHeart(e, npcWorld.EntityManager.ActiveEntities, gameStates, assets, gameStates.Target);
                                                        break;
                                                    case GameStates.Weapons.Crossbow_Attack:
                                                        SystemWeapon.ProcessArrow(e, enemyWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                                        SystemWeapon.ProcessArrow(e, npcWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                                        break;
                                                    case GameStates.Weapons.Crossbow_Sleep:
                                                        SystemWeapon.ProcessSleepArrow(e, enemyWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                                        SystemWeapon.ProcessSleepArrow(e, npcWorld.EntityManager.ActiveEntities, assets, assets.map.Layers["Blocking"]);
                                                        break;
                                                }
                                            }
                                            break;
                                    }
                                }
                                else if (gameStates.LastKey.IsKeyUp(config.ButtonA) && gameStates.NewKey.IsKeyDown(config.ButtonA))
                                {
                                    Vector2 direction = new Vector2();
                                    switch (e.GetComponent<ArtemisComponents.Transform>().Rotation)
                                    {
                                        case 0:
                                            direction = new Vector2(0, 1);
                                            break;
                                        case 1:
                                            direction = new Vector2(-1, 0);
                                            break;
                                        case 2:
                                            direction = new Vector2(1, 0);
                                            break;
                                        case 3:
                                            direction = new Vector2(0, -1);
                                            break;
                                    }
                                    foreach (Entity o in enemyWorld.EntityManager.ActiveEntities)
                                    {
                                        if (o.GetComponent<ArtemisComponents.Transform>().Position == e.GetComponent<ArtemisComponents.Transform>().Position + direction)
                                        {
                                            o.GetComponent<ArtemisComponents.NPC>().HasBeenKilled = true;
                                        }
                                    }
                                    foreach (Entity o in npcWorld.EntityManager.ActiveEntities)
                                    {
                                        if (o.GetComponent<ArtemisComponents.Transform>().Position == e.GetComponent<ArtemisComponents.Transform>().Position + direction)
                                        {
                                            o.GetComponent<ArtemisComponents.NPC>().HasBeenKilled = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (gameStates.LastKey.IsKeyUp(config.ButtonSELECT) && gameStates.NewKey.IsKeyDown(config.ButtonSELECT))
                        {
                            gameStates.GameplayState = GameStates.PlayStates.Journal;
                            gameStates.LastTime = MediaPlayer.PlayPosition;
                            MediaPlayer.Stop();
                        }
                    }
                    else
                    {
                        gameStates.KeyReset = Math.Min(gameStates.KeyReset + 1, 15);
                    }
                    gameStates.LastKey = gameStates.NewKey;

                    //Update Systems
                    foreach (Entity e in world.EntityManager.ActiveEntities)
                    {
                        if (gameStates.PlayerVeloc.X > 1 || gameStates.PlayerVeloc.Y > 1)
                        {
                            SystemPlayerMovement.Process(e, 1 / 7.5f, ref gameStates.PlayerVeloc, gameStates.PayerRot, assets.map.Layers["Blocking"]);
                        }
                        else
                        {
                            SystemPlayerMovement.Process(e, 1 / 30f, ref gameStates.PlayerVeloc, gameStates.PayerRot, assets.map.Layers["Blocking"]);
                        }

                        if (e.GetComponent<ArtemisComponents.Equipment>().ManaTime < 1)
                        {
                            e.GetComponent<ArtemisComponents.Equipment>().IncrementTime(1f / (60f * e.GetComponent<ArtemisComponents.Equipment>().GetLastPowerLevel));
                        }
                        else if (e.GetComponent<ArtemisComponents.Equipment>().ManaTime > 1)
                        {
                            e.GetComponent<ArtemisComponents.Equipment>().ResetTime();
                        }

                        if (e.GetComponent<ArtemisComponents.Equipment>().WeaponTime < 1)
                        {
                            e.GetComponent<ArtemisComponents.Equipment>().IncrementWeaponTime(1f / 180f);
                        }
                        else if (e.GetComponent<ArtemisComponents.Equipment>().WeaponTime > 1)
                        {
                            e.GetComponent<ArtemisComponents.Equipment>().ResetWeaponTime();
                        }

                        if (e.GetComponent<ArtemisComponents.Equipment>().ManaTime == 1f && e.GetComponent<ArtemisComponents.Equipment>().WeaponTime == 1f)
                        {
                            foreach (Entity o in enemyWorld.EntityManager.ActiveEntities)
                            {
                                if (o.GetComponent<ArtemisComponents.NPC>().HasBeenKilled || o.GetComponent<ArtemisComponents.NPC>().IsUnconscious)
                                {
                                    gameStates.GameplayState = GameStates.PlayStates.EndGame;
                                }
                            }
                            foreach (Entity o in npcWorld.EntityManager.ActiveEntities)
                            {
                                if (o.GetComponent<ArtemisComponents.NPC>().HasBeenKilled || o.GetComponent<ArtemisComponents.NPC>().IsUnconscious)
                                {
                                    gameStates.GameplayState = GameStates.PlayStates.EndGame;
                                }
                            }
                        }
                    }
                    break;
                case GameStates.PlayStates.Journal:
                    //Update Music
                    if (MediaPlayer.State == MediaState.Stopped)
                    {
                        MediaPlayer.Play(assets.MenuTheme);
                        MediaPlayer.Volume = config.MusicVolume / 100f;
                    }

                    //Update Input
                    gameStates.NewKey = Keyboard.GetState();
                    if (gameStates.KeyReset == 15)
                    {
                        if (gameStates.LastKey.IsKeyUp(config.ButtonSELECT) && gameStates.NewKey.IsKeyDown(config.ButtonSELECT))
                        {
                            gameStates.GameplayState = GameStates.PlayStates.Playing;
                            MediaPlayer.Stop();
                        }
                    }
                    else
                    {
                        gameStates.KeyReset = Math.Min(gameStates.KeyReset + 1, 15);
                    }
                    gameStates.LastKey = gameStates.NewKey;
                    break;
                case GameStates.PlayStates.EndGame:
                    if (MediaPlayer.State == MediaState.Stopped)
                    {
                        MediaPlayer.Play(assets.MenuTheme);
                        MediaPlayer.Volume = config.MusicVolume / 100f;
                    }
                    break;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //Finite State Machine
            switch (gameStates.GameplayState)
            {
                case GameStates.PlayStates.MainMenu:
                    GraphicsDevice.SetRenderTarget(BackLayer);
                    GraphicsDevice.Clear(new Color(26, 26, 26));
                    spriteBatch.Begin(samplerState: SamplerState.PointClamp);
                    spriteBatch.Draw(assets.Logo, new Rectangle((240 / 2) - (208 / 2), 16, 208, (int)(assets.Logo.Height * (208f / 1024f))), Color.White);
                    spriteBatch.Draw(assets.Cursor, new Vector2(240 - assets.Cursor.Width * 3, 160 - 8 - (assets.MenuFont.MeasureString("N").Y * 0.75f * (2 - (int)gameStates.SelectedMenuItem))), rotation: -90 / 180f * 3.14f, color: Color.White);
                    spriteBatch.DrawString(assets.MenuFont, "New Game", new Vector2(16, 160 - 32 - 48), Color.White);
                    spriteBatch.DrawString(assets.MenuFont, "Load Game", new Vector2(16, 160 - 32 - 24), Color.White);
                    spriteBatch.DrawString(assets.MenuFont, "Options", new Vector2(16, 160 - 32), Color.White);
                    spriteBatch.End();
                    break;
                case GameStates.PlayStates.Options:
                    GraphicsDevice.SetRenderTarget(BackLayer);
                    spriteBatch.Begin(samplerState: SamplerState.PointClamp);
                    spriteBatch.Draw(assets.Logo, new Rectangle((240 / 2) - (208 / 2), 16, 208, (int)(assets.Logo.Height * (208f / 1024f))), Color.White);
                    spriteBatch.Draw(assets.Cursor, new Vector2(240 - assets.Cursor.Width - 16, 160 - 8 - (assets.MenuFont.MeasureString("S").Y * 0.75f * (2 - (int)gameStates.SelectedOptionItem))), rotation: -90 / 180f * 3.14f, color: Color.White);
                    spriteBatch.DrawString(assets.MenuFont, "Sound Volume", new Vector2(16, 160 - 32 - 48), Color.Lerp(Color.White, new Color(204, 0, 0), config.SoundVolume / 100f));
                    spriteBatch.DrawString(assets.MenuFont, "Music Volume", new Vector2(16, 160 - 32 - 24), Color.Lerp(Color.White, new Color(0, 99, 204), config.MusicVolume / 100f));
                    spriteBatch.DrawString(assets.MenuFont, "Back", new Vector2(16, 160 - 32), Color.White);
                    spriteBatch.End();
                    break;
                case GameStates.PlayStates.Playing:
                case GameStates.PlayStates.Quickwheel:
                    Vector2 cameraPos = new Vector2();
                    foreach (Entity e in world.EntityManager.ActiveEntities)
                    {
                        cameraPos = -(e.GetComponent<ArtemisComponents.Transform>().Position * 16) + new Vector2(240 / 2f, 160 / 2f) - new Vector2(8, 8);
                    }

                    GraphicsDevice.SetRenderTarget(BackLayer);
                    GraphicsDevice.Clear(Color.Black);
                    spriteBatch.Begin(samplerState: SamplerState.PointClamp);
                    foreach (TmxLayerTile t in assets.map.Layers["Base"].Tiles)
                    {
                        if (t.Gid > 0)
                        {
                            spriteBatch.Draw(assets.Tileset, new Vector2(t.X * 16, t.Y * 16) + cameraPos, new Rectangle(((t.Gid - 1) % 16) * 16, ((t.Gid - 1) / 16) * 16, 16, 16), Color.White);
                        }
                    }
                    foreach (TmxLayerTile t in assets.map.Layers["Overlay"].Tiles)
                    {
                        if (t.Gid > 0)
                        {
                            spriteBatch.Draw(assets.Tileset, new Vector2(t.X * 16, t.Y * 16) + cameraPos, new Rectangle(((t.Gid - 1) % 16) * 16, ((t.Gid - 1) / 16) * 16, 16, 16), Color.White);
                        }
                    }
                    spriteBatch.End();

                    //Draw Object Layer
                    GraphicsDevice.SetRenderTarget(ObjectLayer);
                    GraphicsDevice.Clear(Color.Transparent);
                    spriteBatch.Begin(samplerState: SamplerState.PointClamp);
                    foreach (TmxLayerTile t in assets.map.Layers["Objects1"].Tiles)
                    {
                        if (t.Gid > 0)
                        {
                            spriteBatch.Draw(assets.Tileset, new Vector2(t.X * 16, t.Y * 16) + cameraPos, new Rectangle(((t.Gid - 1) % 16) * 16, ((t.Gid - 1) / 16) * 16, 16, 16), Color.White);
                        }
                    }
                    foreach (TmxLayerTile t in assets.map.Layers["Objects2"].Tiles)
                    {
                        if (t.Gid > 0)
                        {
                            spriteBatch.Draw(assets.Tileset, new Vector2(t.X * 16, t.Y * 16) + cameraPos, new Rectangle(((t.Gid - 1) % 16) * 16, ((t.Gid - 1) / 16) * 16, 16, 16), Color.White);
                        }
                    }
                    foreach (Entity e in world.EntityManager.ActiveEntities)
                    {
                        foreach (Entity g in enemyWorld.EntityManager.ActiveEntities)
                        {
                            if (g.GetComponent<ArtemisComponents.Transform>().Position.Y <= e.GetComponent<ArtemisComponents.Transform>().Position.Y)
                            {
                                if (g.GetComponent<ArtemisComponents.NPC>().HasBeenKilled && world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().ManaTime < 1f && world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().CurrentPower == GameStates.ActivePowers.DevouringSwarm)
                                {
                                    spriteBatch.Draw(assets.RatsSprite, g.GetComponent<ArtemisComponents.Transform>().Position * 16 + cameraPos, new Rectangle(16 + (16 * (int)Math.Round(Math.Sin(gameTime.TotalGameTime.Ticks / 180f * Math.PI))), 0, 16, 16), Color.White * (1f - world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().ManaTime));
                                }

                                Vector2 o = g.GetComponent<ArtemisComponents.Transform>().Position;
                                spriteBatch.Draw(g.GetComponent<ArtemisComponents.Sprite>().SpriteTexture, new Vector2((int)(o.X * 16), (int)(o.Y * 16) - 16) + cameraPos, new Rectangle(16 + (16 * g.GetComponent<ArtemisComponents.Sprite>().AnimFrame), 32 * g.GetComponent<ArtemisComponents.Transform>().Rotation, 16, 32), Color.White);
                            }
                        }
                        foreach (Entity g in npcWorld.EntityManager.ActiveEntities)
                        {
                            if (g.GetComponent<ArtemisComponents.Transform>().Position.Y <= e.GetComponent<ArtemisComponents.Transform>().Position.Y)
                            {
                                if (g.GetComponent<ArtemisComponents.NPC>().HasBeenKilled && world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().ManaTime < 1f && world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().CurrentPower == GameStates.ActivePowers.DevouringSwarm)
                                {
                                    spriteBatch.Draw(assets.RatsSprite, g.GetComponent<ArtemisComponents.Transform>().Position * 16 + cameraPos, new Rectangle(16 + (16 * (int)Math.Round(Math.Sin(gameTime.TotalGameTime.Ticks / 180f * Math.PI))), 0, 16, 16), Color.White * (1f - world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().ManaTime));
                                }

                                Vector2 o = g.GetComponent<ArtemisComponents.Transform>().Position;
                                spriteBatch.Draw(g.GetComponent<ArtemisComponents.Sprite>().SpriteTexture, new Vector2((int)(o.X * 16), (int)(o.Y * 16) - 16) + cameraPos, new Rectangle(16 + (16 * g.GetComponent<ArtemisComponents.Sprite>().AnimFrame), 32 * g.GetComponent<ArtemisComponents.Transform>().Rotation, 16, 32), Color.White);
                            }
                        }
                        if (e.GetComponent<ArtemisComponents.Equipment>().WeaponTime < 0.1f)
                        {
                            SystemAnimation.DrawFrame(e, spriteBatch, 1);
                        }
                        else
                        {
                            SystemAnimation.Draw(e, spriteBatch);
                        }
                        foreach (Entity g in enemyWorld.EntityManager.ActiveEntities)
                        {
                            if (g.GetComponent<ArtemisComponents.Transform>().Position.Y > e.GetComponent<ArtemisComponents.Transform>().Position.Y)
                            {
                                if (g.GetComponent<ArtemisComponents.NPC>().HasBeenKilled && world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().ManaTime < 1f && world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().CurrentPower == GameStates.ActivePowers.DevouringSwarm)
                                {
                                    spriteBatch.Draw(assets.RatsSprite, g.GetComponent<ArtemisComponents.Transform>().Position * 16 + cameraPos, new Rectangle(16 + (16 * (int)Math.Round(Math.Sin(gameTime.TotalGameTime.Ticks / 180f * Math.PI))), 0, 16, 16), Color.White * (1f - world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().ManaTime));
                                }

                                Vector2 o = g.GetComponent<ArtemisComponents.Transform>().Position;
                                spriteBatch.Draw(g.GetComponent<ArtemisComponents.Sprite>().SpriteTexture, new Vector2((int)(o.X * 16), (int)(o.Y * 16) - 16) + cameraPos, new Rectangle(16 + (16 * g.GetComponent<ArtemisComponents.Sprite>().AnimFrame), 32 * g.GetComponent<ArtemisComponents.Transform>().Rotation, 16, 32), Color.White);
                            }
                        }
                        foreach (Entity g in npcWorld.EntityManager.ActiveEntities)
                        {
                            if (g.GetComponent<ArtemisComponents.Transform>().Position.Y > e.GetComponent<ArtemisComponents.Transform>().Position.Y)
                            {
                                if (g.GetComponent<ArtemisComponents.NPC>().HasBeenKilled && world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().ManaTime < 1f && world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().CurrentPower == GameStates.ActivePowers.DevouringSwarm)
                                {
                                    spriteBatch.Draw(assets.RatsSprite, g.GetComponent<ArtemisComponents.Transform>().Position * 16 + cameraPos, new Rectangle(16 + (16 * (int)Math.Round(Math.Sin(gameTime.TotalGameTime.Ticks / 180f * Math.PI))), 0, 16, 16), Color.White * (1f - world.GetEntity(0).GetComponent<ArtemisComponents.Equipment>().ManaTime));
                                }

                                Vector2 o = g.GetComponent<ArtemisComponents.Transform>().Position;
                                spriteBatch.Draw(g.GetComponent<ArtemisComponents.Sprite>().SpriteTexture, new Vector2((int)(o.X * 16), (int)(o.Y * 16) - 16) + cameraPos, new Rectangle(16 + (16 * g.GetComponent<ArtemisComponents.Sprite>().AnimFrame), 32 * g.GetComponent<ArtemisComponents.Transform>().Rotation, 16, 32), Color.White);
                            }
                        }
                    }
                    spriteBatch.End();

                    //Draw Over Layer
                    GraphicsDevice.SetRenderTarget(OverLayer);
                    GraphicsDevice.Clear(Color.Transparent);
                    spriteBatch.Begin(samplerState: SamplerState.PointClamp);
                    foreach (TmxLayerTile t in assets.map.Layers["AboveWall"].Tiles)
                    {
                        if (t.Gid > 0)
                        {
                            spriteBatch.Draw(assets.Tileset, new Vector2(t.X * 16, t.Y * 16) + cameraPos, new Rectangle(((t.Gid - 1) % 16) * 16, ((t.Gid - 1) / 16) * 16, 16, 16), Color.White);
                        }
                    }
                    spriteBatch.Draw(assets.HUD, new Vector2(0, 0), Color.White);
                    spriteBatch.Draw(assets.Bars, new Vector2((240 / 2f) - 3.5f * 16 - 8, 0), new Rectangle(0, 0, 7 * 16, 16), Color.White);
                    foreach (Entity e in world.EntityManager.ActiveEntities)
                    {
                        float manaBar = e.GetComponent<ArtemisComponents.Equipment>().ManaTime;
                        spriteBatch.Draw(assets.Bars, new Vector2((240 / 2f) - 3.5f * 16 + 8 + (7 * 16) - (int)(7 * 16 * manaBar), 0), new Rectangle((7 * 16) - (int)(7 * 16 * manaBar), 16, (int)((7 * 16) * manaBar), 16), Color.White);
                        if (e.GetComponent<ArtemisComponents.Equipment>().IsWeaponEquipped)
                        {
                            spriteBatch.Draw(assets.SwordIcon, new Vector2(240 - 32, 160 - 48), Color.White);
                            switch (e.GetComponent<ArtemisComponents.Equipment>().SelectedAbility)
                            {
                                case GameStates.EquippedAbility.Weapon:
                                    spriteBatch.Draw(assets.Icons, new Vector2(240 - 48, 160 - 32), new Rectangle(16 * (int)e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon, 16, 16, 16), Color.White);
                                    break;
                                case GameStates.EquippedAbility.Power:
                                    spriteBatch.Draw(assets.Icons, new Vector2(240 - 48, 160 - 32), new Rectangle(16 * (int)e.GetComponent<ArtemisComponents.Equipment>().SelectedPower, 0, 16, 16), Color.White);
                                    break;
                            }
                        }
                        if (e.GetComponent<ArtemisComponents.Equipment>().ManaTime < 1f && e.GetComponent<ArtemisComponents.Equipment>().CurrentPower == GameStates.ActivePowers.DarkVision)
                        {
                            SystemPower.ProcessDarkVision(e, npcWorld.EntityManager.ActiveEntities, spriteBatch, assets.Cursor, e.GetComponent<ArtemisComponents.Equipment>().ManaTime);
                        }
                        if (e.GetComponent<ArtemisComponents.Equipment>().CurrentWeapon == GameStates.Weapons.Heart)
                        {
                            spriteBatch.DrawString(assets.CommonFont, gameStates.LastHint, new Vector2(240 / 2f - assets.CommonFont.MeasureString(gameStates.LastHint).X / 2f + 1, 160 - 16), Color.Black * (1 - e.GetComponent<ArtemisComponents.Equipment>().WeaponTime));
                            spriteBatch.DrawString(assets.CommonFont, gameStates.LastHint, new Vector2(240 / 2f - assets.CommonFont.MeasureString(gameStates.LastHint).X / 2f - 1, 160 - 16), Color.Black * (1 - e.GetComponent<ArtemisComponents.Equipment>().WeaponTime));
                            spriteBatch.DrawString(assets.CommonFont, gameStates.LastHint, new Vector2(240 / 2f - assets.CommonFont.MeasureString(gameStates.LastHint).X / 2f, 160 - 16 + 1), Color.Black * (1 - e.GetComponent<ArtemisComponents.Equipment>().WeaponTime));
                            spriteBatch.DrawString(assets.CommonFont, gameStates.LastHint, new Vector2(240 / 2f - assets.CommonFont.MeasureString(gameStates.LastHint).X / 2f, 160 - 16 - 1), Color.Black * (1 - e.GetComponent<ArtemisComponents.Equipment>().WeaponTime));
                            spriteBatch.DrawString(assets.CommonFont, gameStates.LastHint, new Vector2(240 / 2f - assets.CommonFont.MeasureString(gameStates.LastHint).X / 2f, 160 - 16), Color.White * (1 - e.GetComponent<ArtemisComponents.Equipment>().WeaponTime));
                        }

                        if (gameStates.GameplayState == GameStates.PlayStates.Quickwheel)
                        {
                            spriteBatch.Draw(assets.EquipmentHUD, new Vector2(0, 0), Color.White);
                            for (int x = -1; x < 2; x++)
                            {
                                for (int y = -1; y < 2; y++)
                                {
                                    if (y != 0)
                                    {
                                        spriteBatch.Draw(assets.Icons, new Vector2((240 / 2f) + x * 64 - 8, (3.5f * 16) + y * 20), new Rectangle(16 + (16 * x), 8 + 8 * y, 16, 16), Color.White);
                                    }
                                }
                            }
                            switch (e.GetComponent<ArtemisComponents.Equipment>().SelectedAbility)
                            {
                                case GameStates.EquippedAbility.Weapon:
                                    spriteBatch.Draw(assets.Selection, new Vector2((240 / 2f) + (int)(e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon - 1) * 64 - 16, (3.5f * 16) + 20 - 8), Color.White);
                                    break;
                                case GameStates.EquippedAbility.Power:
                                    spriteBatch.Draw(assets.Selection, new Vector2((240 / 2f) + (int)(e.GetComponent<ArtemisComponents.Equipment>().SelectedPower - 1) * 64 - 16, (3.5f * 16) - 20 - 8), Color.White);
                                    break;
                            }
                            switch (e.GetComponent<ArtemisComponents.Equipment>().SelectedAbility)
                            {
                                case GameStates.EquippedAbility.Power:
                                    switch (e.GetComponent<ArtemisComponents.Equipment>().SelectedPower)
                                    {
                                        case GameStates.ActivePowers.Blink:
                                            spriteBatch.DrawString(assets.MenuFont, "Warp", new Vector2(240 / 2f - assets.MenuFont.MeasureString("Warp").X / 2f, 160 - 32), Color.White);
                                            break;
                                        case GameStates.ActivePowers.DarkVision:
                                            spriteBatch.DrawString(assets.MenuFont, "Sixth Sense", new Vector2(240 / 2f - assets.MenuFont.MeasureString("Sixth Sense").X / 2f, 160 - 32), Color.White);
                                            break;
                                        case GameStates.ActivePowers.DevouringSwarm:
                                            spriteBatch.DrawString(assets.MenuFont, "Infestation", new Vector2(240 / 2f - assets.MenuFont.MeasureString("Infestation").X / 2f, 160 - 32), Color.White);
                                            break;
                                    }
                                    break;
                                case GameStates.EquippedAbility.Weapon:
                                    switch (e.GetComponent<ArtemisComponents.Equipment>().SelectedWeapon)
                                    {
                                        case GameStates.Weapons.Heart:
                                            spriteBatch.DrawString(assets.MenuFont, "Hidden Insight", new Vector2(240 / 2f - assets.MenuFont.MeasureString("Hidden Insight").X / 2f, 160 - 32), Color.White);
                                            break;
                                        case GameStates.Weapons.Crossbow_Attack:
                                            spriteBatch.DrawString(assets.MenuFont, "Poison Arrow", new Vector2(240 / 2f - assets.MenuFont.MeasureString("Poison Arrow").X / 2f, 160 - 32), Color.White);
                                            break;
                                        case GameStates.Weapons.Crossbow_Sleep:
                                            spriteBatch.DrawString(assets.MenuFont, "Sleep Arrow", new Vector2(240 / 2f - assets.MenuFont.MeasureString("Sleep Arrow").X / 2f, 160 - 32), Color.White);
                                            break;
                                    }
                                    break;
                            }
                        }
                    }
                    spriteBatch.End();
                    break;
                case GameStates.PlayStates.Journal:
                    GraphicsDevice.SetRenderTarget(OverLayer);
                    GraphicsDevice.Clear(new Color(26, 26, 26));
                    spriteBatch.Begin(samplerState: SamplerState.PointClamp);
                    spriteBatch.DrawString(assets.MenuFont, "Journal", new Vector2(16, 16), Color.White);
                    spriteBatch.DrawString(assets.CommonFont, "You've learned that His Lordship's strongest", new Vector2(16, 48), Color.White);
                    spriteBatch.DrawString(assets.CommonFont, "supporter is Lady Tudor. However, three", new Vector2(16, 48 + 12), Color.White);
                    spriteBatch.DrawString(assets.CommonFont, "women in the Tudor Family are referred to as", new Vector2(16, 48 + (12 * 2)), Color.White);
                    spriteBatch.DrawString(assets.CommonFont, "Lady Tudor: The wife of the late Lord Tudor", new Vector2(16, 48 + (12 * 3)), Color.White);
                    spriteBatch.DrawString(assets.CommonFont, "and her sisters. You're being sent to the posh", new Vector2(16, 48 + (12 * 4)), Color.White);
                    spriteBatch.DrawString(assets.CommonFont, "Tudor Estate on the night of a costume party in", new Vector2(16, 48 + (12 * 5)), Color.White);
                    spriteBatch.DrawString(assets.CommonFont, "order to determine which Lady Tudor", new Vector2(16, 48 + (12 * 6)), Color.White);
                    spriteBatch.DrawString(assets.CommonFont, "you must eliminate.", new Vector2(16, 48 + (12 * 7)), Color.White);
                    spriteBatch.DrawString(assets.CommonFont, "Press SEL to continue...", new Vector2(240 - 16 - assets.CommonFont.MeasureString("Press SEL to continue...").X, 48 + (12 * 7)), Color.White);
                    spriteBatch.End();
                    break;
                case GameStates.PlayStates.EndGame:
                    GraphicsDevice.SetRenderTarget(OverLayer);
                    GraphicsDevice.Clear(new Color(26, 26, 26));
                    spriteBatch.Begin(samplerState: SamplerState.PointClamp);
                    spriteBatch.DrawString(assets.MenuFont, "Game Over", new Vector2(240 / 2f - assets.MenuFont.MeasureString("Game Over").X / 2f, 160 / 4f), Color.White);
                    foreach (Entity e in enemyWorld.EntityManager.ActiveEntities)
                    {
                        if (e.GetComponent<ArtemisComponents.NPC>().HasBeenKilled)
                        {
                            spriteBatch.DrawString(assets.CommonFont, "A Guard has been killed.", new Vector2(240 / 2f - assets.CommonFont.MeasureString("A Guard has been killed.").X / 2f, (160 / 4f) * 2f), new Color(204, 0, 0));
                        }
                        else if (e.GetComponent<ArtemisComponents.NPC>().IsUnconscious)
                        {
                            spriteBatch.DrawString(assets.CommonFont, "A Guard has been knocked out.", new Vector2(240 / 2f - assets.CommonFont.MeasureString("A Guard has been knocked out.").X / 2f, (160 / 4f) * 2f), new Color(204, 0, 0));
                        }
                    }
                    foreach (Entity e in npcWorld.EntityManager.ActiveEntities)
                    {
                        if (e.HasComponent<ArtemisComponents.Hint>())
                        {

                            if (e.GetComponent<ArtemisComponents.NPC>().HasBeenKilled)
                            {
                                spriteBatch.DrawString(assets.CommonFont, "A Guest has been killed.", new Vector2(240 / 2f - assets.CommonFont.MeasureString("A Guest has been killed.").X / 2f, (160 / 4f) * 2f), new Color(204, 0, 0));
                            }
                            else if (e.GetComponent<ArtemisComponents.NPC>().IsUnconscious)
                            {
                                spriteBatch.DrawString(assets.CommonFont, "A Guest has been knocked out.", new Vector2(240 / 2f - assets.CommonFont.MeasureString("A Guest has been knocked out.").X / 2f, (160 / 4f) * 2f), new Color(204, 0, 0));
                            }
                        }
                        else if (e.HasComponent<ArtemisComponents.Target>())
                        {
                            if (e.GetComponent<ArtemisComponents.NPC>().Name.Contains((gameStates.Target + 1).ToString()))
                            {
                                if (e.GetComponent<ArtemisComponents.NPC>().HasBeenKilled)
                                {
                                    spriteBatch.DrawString(assets.CommonFont, "Lady Tudor has been killed.", new Vector2(240 / 2f - assets.CommonFont.MeasureString("Lady Tudor has been killed.").X / 2f, (160 / 4f) * 2f), new Color(0, 100, 204));
                                }
                                else if (e.GetComponent<ArtemisComponents.NPC>().IsUnconscious)
                                {
                                    spriteBatch.DrawString(assets.CommonFont, "Lady Tudor has been knocked out.", new Vector2(240 / 2f - assets.CommonFont.MeasureString("Lady Tudor has been knocked out.").X / 2f, (160 / 4f) * 2f), new Color(0, 100, 204));
                                }
                            }
                            else
                            {
                                if (e.GetComponent<ArtemisComponents.NPC>().HasBeenKilled)
                                {
                                    spriteBatch.DrawString(assets.CommonFont, "The wrong Lady Tudor has been killed.", new Vector2(240 / 2f - assets.CommonFont.MeasureString("The wrong Lady Tudor has been killed.").X / 2f, (160 / 4f) * 2f), new Color(204, 0, 0));
                                }
                                else if (e.GetComponent<ArtemisComponents.NPC>().IsUnconscious)
                                {
                                    spriteBatch.DrawString(assets.CommonFont, "The wrong Lady Tudor has been knocked out.", new Vector2(240 / 2f - assets.CommonFont.MeasureString("The wrong Lady Tudor has been knocked out.").X / 2f, (160 / 4f) * 2f), new Color(204, 0, 0));
                                }
                            }
                        }
                    }
                    spriteBatch.DrawString(assets.CommonFont, "Press SEL to return to the Main Menu...", new Vector2(240 / 2f - assets.CommonFont.MeasureString("Press SEL to return to the Main Menu...").X / 2f, (160 / 4f) * 3f), Color.White);
                    spriteBatch.End();
                    break;
            }

            //Draw To Screen
            GraphicsDevice.SetRenderTarget(null);
            graphics.PreferredBackBufferWidth = 240 * config.ResScale;
            graphics.PreferredBackBufferHeight = 160 * config.ResScale;
            graphics.ApplyChanges();

            spriteBatch.Begin(samplerState: SamplerState.PointClamp);
            spriteBatch.Draw(BackLayer, new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.White);
            spriteBatch.Draw(ObjectLayer, new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.White);
            spriteBatch.Draw(OverLayer, new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
