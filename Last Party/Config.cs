﻿using System.IO;
using Microsoft.Xna.Framework.Input;

namespace Last_Party
{
    public class Config
    {
        //Paths
        public string ContentPath;
        public string MapsPath;
        public string SpritesPath;
        public string TexturesPath;
        public string SoundsPath;
        public string MusicPath;

        //Controls
        public Keys ButtonSTART = Keys.Y;
        public Keys ButtonSELECT = Keys.R;

        public Keys ButtonA = Keys.L;
        public Keys ButtonB = Keys.K;
        public Keys ButtonL = Keys.Q;
        public Keys ButtonR = Keys.O;

        public Keys ButtonUP = Keys.W;
        public Keys ButtonDOWN = Keys.S;
        public Keys ButtonLEFT = Keys.A;
        public Keys ButtonRIGHT = Keys.D;

        //Scaling
        public int SoundVolume;
        public int MusicVolume;
        public int ResScale;

        //Constructor
        public Config()
        {
            ContentPath = @"\Content\";
            MapsPath = ContentPath + @"Maps\";
            SpritesPath = ContentPath + @"Sprites\";
            TexturesPath = ContentPath + @"Textures\";
            SoundsPath = ContentPath + @"Sounds\";
            MusicPath = ContentPath + @"Music\";

            ButtonSTART = Keys.Y;
            ButtonSELECT = Keys.R;

            ButtonA = Keys.L;
            ButtonB = Keys.K;
            ButtonL = Keys.Q;
            ButtonR = Keys.O;

            ButtonUP = Keys.W;
            ButtonDOWN = Keys.S;
            ButtonLEFT = Keys.A;
            ButtonRIGHT = Keys.D;

            SoundVolume = 100;
            MusicVolume = 100;
            ResScale = 1;

            if (!Directory.Exists(Directory.GetCurrentDirectory() + ContentPath))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + ContentPath);
            }
            if (!Directory.Exists(Directory.GetCurrentDirectory() + MapsPath))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + MapsPath);
            }
            if (!Directory.Exists(Directory.GetCurrentDirectory() + SpritesPath))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + SpritesPath);
            }
            if (!Directory.Exists(Directory.GetCurrentDirectory() + TexturesPath))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + TexturesPath);
            }
            if (!Directory.Exists(Directory.GetCurrentDirectory() + SoundsPath))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + SoundsPath);
            }
            if (!Directory.Exists(Directory.GetCurrentDirectory() + MusicPath))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + MusicPath);
            }
        }
    }
}
