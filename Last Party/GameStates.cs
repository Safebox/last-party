﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Last_Party
{
    public class GameStates
    {
        #region Enums
        //Menu
        public enum MainMenuStates { NewGame, LoadGame, Options };
        public enum OptionStates { SoundVolume, MusicVolume, Back };
        public enum PlayStates { MainMenu, Options, Playing, Journal, Quickwheel, EndGame };

        //Weapons and Powers
        public enum EquippedAbility { Weapon, Power };
        public enum Weapons { Heart, Crossbow_Attack, Crossbow_Sleep, Grenade, Pistol, Springrazor };
        public enum ActivePowers { Blink, DarkVision, DevouringSwarm, Possession, BendTime, WindBlast};
        public enum PassivePowers { Vitality, BloodThirsty, Agility, ShadowKill};
        #endregion

        #region Variables
        public KeyboardState LastKey;
        public KeyboardState NewKey;
        public int KeyReset;
        public MainMenuStates SelectedMenuItem;
        public OptionStates SelectedOptionItem;
        public PlayStates GameplayState;
        public int PayerRot;
        public Vector2 PlayerVeloc;
        public int CurrentSong;
        public TimeSpan LastTime;
        public int Target;
        public string LastHint;
        #endregion

        public GameStates()
        {
            LastKey = new KeyboardState();
            NewKey = new KeyboardState();
            KeyReset = 15;
            SelectedMenuItem = MainMenuStates.NewGame;
            GameplayState = PlayStates.MainMenu;
            PayerRot = 3;
            PlayerVeloc = new Vector2();
            CurrentSong = 0;
            LastTime = new TimeSpan();
            Target = 0;
            LastHint = "";
        }
    }
}
