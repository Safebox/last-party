﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using TiledSharp;

namespace Last_Party
{
    public class AssetLoader
    {
        //Fonts
        public SpriteFont MenuFont;
        public SpriteFont CommonFont;
        public string[] Target1Hints;
        public string[] Target2Hints;
        public string[] Target3Hints;

        //Maps
        public TmxMap map;

        //Textures
        public Texture2D Logo;
        public Texture2D Cursor;
        public Texture2D HUD;
        public Texture2D EquipmentHUD;
        public Texture2D Selection;
        public Texture2D Bars;
        public Texture2D Icons;
        public Texture2D SwordIcon;
        public Texture2D Tileset;

        //Sprites
        public Texture2D PlayerSprite;
        public Texture2D GuardSprite;
        public Texture2D MusicGuardSprite;
        public Texture2D Guest1Sprite;
        public Texture2D Guest2Sprite;
        public Texture2D Guest3Sprite;
        public Texture2D Target1Sprite;
        public Texture2D Target2Sprite;
        public Texture2D Target3Sprite;
        
        public Texture2D RatsSprite;

        //Sounds

        //Music
        public Song MenuTheme;
        public Song BackgroundMusic1;
        public Song BackgroundMusic2;

        public AssetLoader(ContentManager content, GraphicsDevice gd, Config config)
        {
            MenuFont = content.Load<SpriteFont>("Menu");
            CommonFont = content.Load<SpriteFont>("Text");
            Target1Hints = new string[] {
                "She is paranoid. And rightly so.",
            "She likes to stand out with vibrancy.",
            "Her 'lover' awaits nearby. He was not invited."};
            Target2Hints = new string[] {
                "She has bedded many men in this room.",
            "Alcohol has stained this carpet, more than once.",
            "It was here she met His Lordship."};
            Target3Hints = new string[] {
                "She has talent beyond compare.",
            "She prefers the company of her equals.",
            "You can hear her now, listen..."};

            map = new TmxMap(Directory.GetCurrentDirectory() + config.MapsPath + "GroundFloor.tmx");

            Logo = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.TexturesPath + "Logo.png", FileMode.Open));
            Cursor = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.TexturesPath + "Cursor.png", FileMode.Open));
            HUD = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.TexturesPath + "HUD.png", FileMode.Open));
            EquipmentHUD = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.TexturesPath + "Quickwheel.png", FileMode.Open));
            Selection = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.TexturesPath + "Selection.png", FileMode.Open));
            Bars = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.TexturesPath + "Bars.png", FileMode.Open));
            Icons = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.TexturesPath + "Equipment.png", FileMode.Open));
            SwordIcon = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.TexturesPath + "Sword.png", FileMode.Open));
            Tileset = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory()+ config.TexturesPath + "Tileset.png", FileMode.Open));
            
            PlayerSprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "Player.png", FileMode.Open));
            
            GuardSprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "Guard.png", FileMode.Open));
            MusicGuardSprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "MusicGuard.png", FileMode.Open));
            Guest1Sprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "Guest1.png", FileMode.Open));
            Guest2Sprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "Guest2.png", FileMode.Open));
            Guest3Sprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "Guest3.png", FileMode.Open));
            Target1Sprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "Target1.png", FileMode.Open));
            Target2Sprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "Target2.png", FileMode.Open));
            Target3Sprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "Target3.png", FileMode.Open));

            RatsSprite = Texture2D.FromStream(gd, new FileStream(Directory.GetCurrentDirectory() + config.SpritesPath + "Rats.png", FileMode.Open));

            MenuTheme = content.Load<Song>("Kai_Engel_-_04_-_Moonlight_Reprise");
            BackgroundMusic1 = content.Load<Song>("HWV 317 - Part 1");
            BackgroundMusic2 = content.Load<Song>("HWV 317 - Part 2");
        }
    }
}
