﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Artemis;
using Artemis.System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledSharp;

namespace Last_Party.ArtemisSystems
{
    public class SystemPlayerMovement : EntityProcessingSystem
    {
        public SystemPlayerMovement() : base(typeof(ArtemisComponents.Transform))
        {

        }

        public void Process(Entity entity, float time, ref Vector2 direction, int rotation, TmxLayer collisionMap)
        {
            entity.GetComponent<ArtemisComponents.Transform>().Rotation = rotation;
            Vector2 newPos = entity.GetComponent<ArtemisComponents.Transform>().LastPosition + direction;
            int tileID = (int)(newPos.Y * 256) + (int)(newPos.X);

            if (collisionMap.Tiles[tileID].Gid > 0)
            {
                if (entity.GetComponent<ArtemisComponents.Transform>().Position != newPos)
                {
                    float adjustedTime = (float)(Math.Sin(entity.GetComponent<ArtemisComponents.Sprite>().AnimTime * Math.PI - Math.PI / 2) + 1) / 2f;
                    entity.GetComponent<ArtemisComponents.Transform>().Position = Vector2.Lerp(entity.GetComponent<ArtemisComponents.Transform>().LastPosition, newPos, entity.GetComponent<ArtemisComponents.Sprite>().AnimTime);
                    entity.GetComponent<ArtemisComponents.Sprite>().IncrementTime(time);
                    if (entity.GetComponent<ArtemisComponents.Sprite>().AnimTime > 1)
                    {
                        entity.GetComponent<ArtemisComponents.Transform>().Position = newPos;
                    }
                }
                else
                {
                    entity.GetComponent<ArtemisComponents.Transform>().Position = new Vector2((int)entity.GetComponent<ArtemisComponents.Transform>().Position.X, (int)entity.GetComponent<ArtemisComponents.Transform>().Position.Y);
                    entity.GetComponent<ArtemisComponents.Transform>().LastPosition = entity.GetComponent<ArtemisComponents.Transform>().Position;
                    entity.GetComponent<ArtemisComponents.Sprite>().ResetTime();
                    direction = new Vector2();
                }
            }
            else
            {
                direction = new Vector2();
            }
        }

        public override void Process(Entity entity)
        {
            throw new NotImplementedException();
        }
    }
}
