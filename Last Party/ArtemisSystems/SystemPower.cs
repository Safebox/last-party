﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Artemis;
using Artemis.System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledSharp;

namespace Last_Party.ArtemisSystems
{
    public class SystemPower : EntityProcessingSystem
    {
        public SystemPower() : base(typeof(ArtemisComponents.Equipment))
        {

        }

        public void ProcessBlink(Entity entity, ref Vector2 direction, TmxLayer blocking)
        {
            switch (entity.GetComponent<ArtemisComponents.Transform>().Rotation)
            {
                case 0:
                    direction = new Vector2(0, 1);
                    break;
                case 1:
                    direction = new Vector2(-1, 0);
                    break;
                case 2:
                    direction = new Vector2(1, 0);
                    break;
                case 3:
                    direction = new Vector2(0, -1);
                    break;
            }
            Vector2 offset = direction;
            for (int mult = 1; mult < 3 * entity.GetComponent<ArtemisComponents.Equipment>().GetLastPowerLevel; mult++)
            {
                offset = direction * mult;
                if (blocking.Tiles[(int)((entity.GetComponent<ArtemisComponents.Transform>().Position.Y + offset.Y) * 256) + (int)(entity.GetComponent<ArtemisComponents.Transform>().Position.X + offset.X)].Gid == 0)
                {
                    offset = direction * (mult - 1);
                    break;
                }
            }
            direction = offset;
            entity.GetComponent<ArtemisComponents.Equipment>().SetLastPower(GameStates.ActivePowers.Blink);
            entity.GetComponent<ArtemisComponents.Equipment>().StartTime(GameStates.ActivePowers.Blink);
        }

        public void ProcessDarkVision(Entity entity, Artemis.Utils.Bag<Entity> others, SpriteBatch sb, Texture2D hintIcon, float strength)
        {
            foreach (Entity e in others)
            {
                Vector2 pos = e.GetComponent<ArtemisComponents.Transform>().Position - entity.GetComponent<ArtemisComponents.Transform>().Position;
                pos.Normalize();
                if (e.HasComponent<ArtemisComponents.Hint>())
                {
                    sb.Draw(hintIcon, new Vector2(240 / 2f, 160 / 2f) + (pos * 3 * 16) - new Vector2(16, 16), Color.White * (1 - strength));
                }
                else if (e.HasComponent<ArtemisComponents.Target>())
                {
                    sb.Draw(hintIcon, new Vector2(240 / 2f, 160 / 2f) + (pos * 3 * 16) - new Vector2(16, 16), new Color(204,0,0) * (1 - strength));
                }
            }
            entity.GetComponent<ArtemisComponents.Equipment>().SetLastPower(GameStates.ActivePowers.DarkVision);
        }

        public void ProcessRats(Entity entity, Artemis.Utils.Bag<Entity> others, AssetLoader assets, TmxLayer blocking)
        {
            Vector2 direction = new Vector2();
            switch (entity.GetComponent<ArtemisComponents.Transform>().Rotation)
            {
                case 0:
                    direction = new Vector2(0, 1);
                    break;
                case 1:
                    direction = new Vector2(-1, 0);
                    break;
                case 2:
                    direction = new Vector2(1, 0);
                    break;
                case 3:
                    direction = new Vector2(0, -1);
                    break;
            }
            Vector2 offset = direction;
            for (int mult = 1; mult < 3 * entity.GetComponent<ArtemisComponents.Equipment>().GetLastPowerLevel; mult++)
            {
                offset = direction * mult;
                if (blocking.Tiles[(int)((entity.GetComponent<ArtemisComponents.Transform>().Position.Y + offset.Y) * 256) + (int)(entity.GetComponent<ArtemisComponents.Transform>().Position.X + offset.X)].Gid == 0)
                {
                    offset = direction * (mult - 1);
                    break;
                }
            }
            foreach (Entity e in others)
            {
                if (e.GetComponent<ArtemisComponents.Transform>().Position == entity.GetComponent<ArtemisComponents.Transform>().Position + direction)
                {
                    e.GetComponent<ArtemisComponents.NPC>().HasBeenKilled = true;
                }
            }
            entity.GetComponent<ArtemisComponents.Equipment>().SetLastPower(GameStates.ActivePowers.DevouringSwarm);
            entity.GetComponent<ArtemisComponents.Equipment>().StartTime(GameStates.ActivePowers.DevouringSwarm);
        }

        public override void Process(Entity entity)
        {
            throw new NotImplementedException();
        }
    }
}
