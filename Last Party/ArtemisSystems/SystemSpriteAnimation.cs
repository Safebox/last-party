﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Artemis;
using Artemis.System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Last_Party.ArtemisSystems
{
    public class SystemSpriteAnimation : EntityProcessingSystem
    {
        public SystemSpriteAnimation() : base(typeof(ArtemisComponents.Sprite))
        {

        }

        public void Draw (Entity entity, SpriteBatch sb)
        {
            sb.Draw(entity.GetComponent<ArtemisComponents.Sprite>().SpriteTexture, new Vector2(240 / 2f - 8, 160 / 2f - 8 - 16), new Rectangle(16 + (16 * entity.GetComponent<ArtemisComponents.Sprite>().AnimFrame), 32 * entity.GetComponent<ArtemisComponents.Transform>().Rotation, 16, 32), Color.White);
        }
        public void DrawFrame(Entity entity, SpriteBatch sb, int frame)
        {
            sb.Draw(entity.GetComponent<ArtemisComponents.Sprite>().SpriteTexture, new Vector2(240 / 2f - 8, 160 / 2f - 8 - 16), new Rectangle(16 + (16 * frame), 32 * entity.GetComponent<ArtemisComponents.Transform>().Rotation, 16, 32), Color.White);
        }

        public override void Process(Entity entity)
        {
            throw new Exception();
        }
    }
}
