﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Artemis;
using Artemis.System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledSharp;

namespace Last_Party.ArtemisSystems
{
    public class SystemWeapon : EntityProcessingSystem
    {
        public SystemWeapon() : base(typeof(ArtemisComponents.Equipment))
        {

        }

        public void ProcessHeart(Entity entity, Artemis.Utils.Bag<Entity> others, GameStates gameStates, AssetLoader assets, int target)
        {
            Vector2 offset = new Vector2();
            switch (entity.GetComponent<ArtemisComponents.Transform>().Rotation)
            {
                case 0:
                    offset = new Vector2(0, 1);
                    break;
                case 1:
                    offset = new Vector2(-1, 0);
                    break;
                case 2:
                    offset = new Vector2(1, 0);
                    break;
                case 3:
                    offset = new Vector2(0, -1);
                    break;
            }
            Vector2 tilePos = entity.GetComponent<ArtemisComponents.Transform>().Position + offset;
            foreach (Entity e in others)
            {
                if (e.GetComponent<ArtemisComponents.Transform>().Position == tilePos && e.HasComponent<ArtemisComponents.Hint>())
                {
                    string text = "";
                    Random r = new Random(e.Id);
                    switch (target)
                    {
                        case 0:
                            text = assets.Target1Hints[r.Next(assets.Target1Hints.Length)];
                            break;
                        case 1:
                            text = assets.Target2Hints[r.Next(assets.Target2Hints.Length)];
                            break;
                        case 2:
                            text = assets.Target3Hints[r.Next(assets.Target3Hints.Length)];
                            break;
                    }
                    gameStates.LastHint = text;
                }
            }
            entity.GetComponent<ArtemisComponents.Equipment>().StartWeaponTime(GameStates.Weapons.Heart);
        }

        public void ProcessArrow(Entity entity, Artemis.Utils.Bag<Entity> others, AssetLoader assets, TmxLayer blocking)
        {
            Vector2 direction = new Vector2();
            switch (entity.GetComponent<ArtemisComponents.Transform>().Rotation)
            {
                case 0:
                    direction = new Vector2(0, 1);
                    break;
                case 1:
                    direction = new Vector2(-1, 0);
                    break;
                case 2:
                    direction = new Vector2(1, 0);
                    break;
                case 3:
                    direction = new Vector2(0, -1);
                    break;
            }
            Vector2 offset = direction;
            for (int mult = 1; mult < 5; mult++)
            {
                offset = direction * mult;
                if (blocking.Tiles[(int)((entity.GetComponent<ArtemisComponents.Transform>().Position.Y + offset.Y) * 256) + (int)(entity.GetComponent<ArtemisComponents.Transform>().Position.X + offset.X)].Gid == 0)
                {
                    offset = direction * mult;
                    break;
                }
            }
            foreach (Entity e in others)
            {
                if (e.GetComponent<ArtemisComponents.Transform>().Position == entity.GetComponent<ArtemisComponents.Transform>().Position + offset)
                {
                    e.GetComponent<ArtemisComponents.NPC>().HasBeenKilled = true;
                }
            }
            entity.GetComponent<ArtemisComponents.Equipment>().StartWeaponTime(GameStates.Weapons.Crossbow_Attack);
        }

        public void ProcessSleepArrow(Entity entity, Artemis.Utils.Bag<Entity> others, AssetLoader assets, TmxLayer blocking)
        {
            Vector2 direction = new Vector2();
            switch (entity.GetComponent<ArtemisComponents.Transform>().Rotation)
            {
                case 0:
                    direction = new Vector2(0, 1);
                    break;
                case 1:
                    direction = new Vector2(-1, 0);
                    break;
                case 2:
                    direction = new Vector2(1, 0);
                    break;
                case 3:
                    direction = new Vector2(0, -1);
                    break;
            }
            Vector2 offset = direction;
            for (int mult = 1; mult < 5; mult++)
            {
                offset = direction * mult;
                if (blocking.Tiles[(int)((entity.GetComponent<ArtemisComponents.Transform>().Position.Y + offset.Y) * 256) + (int)(entity.GetComponent<ArtemisComponents.Transform>().Position.X + offset.X)].Gid == 0)
                {
                    offset = direction * mult;
                    break;
                }
            }
            foreach (Entity e in others)
            {
                if (e.GetComponent<ArtemisComponents.Transform>().Position == entity.GetComponent<ArtemisComponents.Transform>().Position + offset)
                {
                    e.GetComponent<ArtemisComponents.NPC>().IsUnconscious = true;
                }
            }
            entity.GetComponent<ArtemisComponents.Equipment>().StartWeaponTime(GameStates.Weapons.Crossbow_Sleep);
        }

        public override void Process(Entity entity)
        {
            throw new NotImplementedException();
        }
    }
}
